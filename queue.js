let collection = [];

// Write the queue functions below.

function print(){
	return collection
}

function enqueue(data){
	collection[collection.length] = data;
    return collection;
}

function dequeue(){
    collection[0] = collection[1];
    collection.length--;
    return collection;
}

function front(){
	return collection[0];
}

function size(){
	return collection.length;
}

function isEmpty(){
	return collection.length === 0;
}

module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};